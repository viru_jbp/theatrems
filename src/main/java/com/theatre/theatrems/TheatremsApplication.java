package com.theatre.theatrems;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TheatremsApplication {

	public static void main(String[] args) {
		SpringApplication.run(TheatremsApplication.class, args);
	}

}
