package com.theatre.theatrems.repositories;

import com.theatre.theatrems.models.Theatre;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TheatreRepository extends JpaRepository<Theatre, Long> {
    // Define custom queries if needed
}
